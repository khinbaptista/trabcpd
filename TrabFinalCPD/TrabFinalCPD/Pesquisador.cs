﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabFinalCPD
{
    class Pesquisador : IComparable<Pesquisador>, IEquatable<Pesquisador>
    {
        public String nome;
        public List<Artigo> artigos;
        public List<int>[] offsetArtigos = new List<int>[5];

        /* Ordenação:
         * 0: por título
         * 1: por tipo
         * 2: por natureza
         * 3: por qualis
         * 4: por número de coautores
         */

        public Pesquisador(String pesq, List<Artigo> listaArtigos)
        {
            nome = pesq;
            artigos = listaArtigos;
        }

        public Pesquisador(String pesq, List<int>[] offsetArticles)
        {
            nome = pesq;

            offsetArtigos = offsetArticles;
        }

        //  Interfaces IComparable:
        public int CompareTo(Pesquisador other)
        {
            return String.Compare(this.nome, other.nome);
        }

        public class SortNameZA : IComparer<Pesquisador>
        {
            public int Compare(Pesquisador X, Pesquisador Y)
            {
                return (-1) * String.Compare(X.nome, Y.nome);
            }
        }

        //  Interface IEquatable:
        public override int GetHashCode()
        {
            return this.nome.GetHashCode();
        }

        public bool Equals(Pesquisador other)
        {
            if (other == null)
                return false;

            return (this.nome.Equals(other.nome));
        }

        public override bool Equals(object obj)
        {
            Pesquisador pesq = obj as Pesquisador;

            return Equals(pesq);
        }
    }

    class Artigo : IComparable<Artigo>, IEquatable<Artigo>
    {
        public enum tipoArtigo {Periodico, Conferencia}

        public enum naturezaArtigo {Completo, Estendido, Resumo}

        public enum QUALIS
        {
            A1, A2,
            B1, B2, B3, B4, B5,
            C,
            Vazio
        }

        String titulo;
        tipoArtigo tipo;
        naturezaArtigo natureza;
        QUALIS qualis;
        int n_coautores;

        public string[] palavraChave = new string[6];

        public Artigo(String title, int type, String nature, string thisQualis, int authors, string[] keywords)
        {
            titulo = title;

            if (type == 1)
                tipo = tipoArtigo.Periodico;
            else if (type == 0)
                tipo = tipoArtigo.Conferencia;

            if (nature == "COMPLETO")
                natureza = naturezaArtigo.Completo;
            else if (nature == "ESTENDIDO")
                natureza = naturezaArtigo.Estendido;
            else if (nature == "RESUMO")
                natureza = naturezaArtigo.Resumo;

            switch (thisQualis)
            {
                case "A1":
                    qualis = QUALIS.A1; break;
                case "A2":
                    qualis = QUALIS.A2; break;
                case "B1":
                    qualis = QUALIS.B1; break;
                case "B2":
                    qualis = QUALIS.B2; break;
                case "B3":
                    qualis = QUALIS.B3; break;
                case "B4":
                    qualis = QUALIS.B4; break;
                case "B5":
                    qualis = QUALIS.B5; break;
                case "C":
                    qualis = QUALIS.C; break;
                default:
                    qualis = QUALIS.Vazio; break;
            }

            n_coautores = authors;

            for (int i = 0; i < 6; i++)
                if (keywords[i] == "" || keywords[i] == null)
                    keywords[i] = "0";
                //else if (keywords[i] == null)


            palavraChave = keywords;
        }

        public Artigo(String title, int type, int nature, string thisQualis, int authors, string[] keywords)
        {
            titulo = title;

            if (type == 1)
                tipo = tipoArtigo.Periodico;
            else if (type == 0)
                tipo = tipoArtigo.Conferencia;

            if (nature == 0)
                natureza = naturezaArtigo.Completo;
            else if (nature == 1)
                natureza = naturezaArtigo.Estendido;
            else if (nature == 2)
                natureza = naturezaArtigo.Resumo;

            switch (thisQualis)
            {
                case "A1":
                    qualis = QUALIS.A1; break;
                case "A2":
                    qualis = QUALIS.A2; break;
                case "B1":
                    qualis = QUALIS.B1; break;
                case "B2":
                    qualis = QUALIS.B2; break;
                case "B3":
                    qualis = QUALIS.B3; break;
                case "B4":
                    qualis = QUALIS.B4; break;
                case "B5":
                    qualis = QUALIS.B5; break;
                case "C":
                    qualis = QUALIS.C; break;
                default:
                    qualis = QUALIS.Vazio; break;
            }

            n_coautores = authors;

            for (int i = 0; i < 6; i++)
                if (keywords[i] == "")
                    keywords[i] = "0";

            for (int j = 0; j < 6; j++) 
            {
                string keyword = keywords[j];
                palavraChave[j] = keyword;
            }
        }

        // Getters:
        public String getTitulo()
        {
            return this.titulo;
        }

        public tipoArtigo getTipo()
        {
            return this.tipo;
        }

        public naturezaArtigo getNatureza()
        {
            return this.natureza;
        }

        public QUALIS getQualis()
        {
            return this.qualis;
        }

        public int getNCoautores()
        {
            return this.n_coautores;
        }

        // Interfaces:
        public string[] ToArrayString()     // pra preencher a dataGrid, sempre usar essa função
        {
            string[] pesq = new string[5];

            pesq[0] = this.titulo;

            if (this.tipo == tipoArtigo.Conferencia)
                pesq[1] = "Conferência";
            else
                pesq[1] = "Periódico";

            if (this.natureza == naturezaArtigo.Completo)
                pesq[2] = "Completo";
            else if (this.natureza == naturezaArtigo.Estendido)
                pesq[2] = "Estendido";
            else
                pesq[2] = "Resumo";

            switch (this.qualis)
            {
                case QUALIS.A1:
                    pesq[3] = "A1"; break;
                case QUALIS.A2:
                    pesq[3] = "A2"; break;
                case QUALIS.B1:
                    pesq[3] = "B1"; break;
                case QUALIS.B2:
                    pesq[3] = "B2"; break;
                case QUALIS.B3:
                    pesq[3] = "B3"; break;
                case QUALIS.B4:
                    pesq[3] = "B4"; break;
                case QUALIS.B5:
                    pesq[3] = "B5"; break;
                case QUALIS.C:
                    pesq[3] = "C"; break;
                default:
                    pesq[3] = "Vazio"; break;
            }
            
            pesq[4] = this.n_coautores.ToString();

            return pesq;
        }

        public override int GetHashCode()
        {
            return this.titulo.GetHashCode();
        }

        public bool Equals(Artigo other)
        {
            if (other == null)
                return false;

            return (this.titulo.Equals(other.titulo));
        }

        public override bool Equals(object obj)
        {
            Artigo article = obj as Artigo;

            return Equals(article);
        }

        public int CompareTo(Artigo other)
        {
            return String.Compare(this.getTitulo(), other.getTitulo());
        }

            // Interfaces de comparação
        public class SortTipo : IComparer<Artigo>
        {
            public int Compare(Artigo X, Artigo Y)
            {
                if (X.getTipo() == Y.getTipo())
                    return 0;
                if (X.getTipo() == tipoArtigo.Conferencia)
                    return -1;
                return 1;
            }
        }

        public class SortNatureza : IComparer<Artigo>
        {
            public int Compare(Artigo X, Artigo Y)
            {
                if (X.getNatureza() == Y.getNatureza())
                    return 0;
                if (X.getNatureza() == naturezaArtigo.Completo)
                    return -1;
                if (X.getNatureza() == naturezaArtigo.Resumo)
                    return 1;
                if (Y.getNatureza() == naturezaArtigo.Completo)
                        return 1;
                return -1;
            }
        }

        public class SortQualis : IComparer<Artigo>
        {
            public int Compare(Artigo X, Artigo Y)
            {
                string x, y;

                if (X.getQualis() == QUALIS.A1) x = "A1";
                else if (X.getQualis() == QUALIS.A2) x = "A2";
                else if (X.getQualis() == QUALIS.B1) x = "B1";
                else if (X.getQualis() == QUALIS.B2) x = "B2";
                else if (X.getQualis() == QUALIS.B3) x = "B3";
                else if (X.getQualis() == QUALIS.B4) x = "B4";
                else if (X.getQualis() == QUALIS.B5) x = "B5";
                else if (X.getQualis() == QUALIS.C) x = "C";
                else x = "Vazio";

                if (Y.getQualis() == QUALIS.A1) y = "A1";
                else if (Y.getQualis() == QUALIS.A2) y = "A2";
                else if (Y.getQualis() == QUALIS.B1) y = "B1";
                else if (Y.getQualis() == QUALIS.B2) y = "B2";
                else if (Y.getQualis() == QUALIS.B3) y = "B3";
                else if (Y.getQualis() == QUALIS.B4) y = "B4";
                else if (Y.getQualis() == QUALIS.B5) y = "B5";
                else if (Y.getQualis() == QUALIS.C) y = "C";
                else y = "Vazio";

                return String.Compare(x, y);

                /*
                if (X.getQualis() == Y.getQualis())     // são diferentes
                    return 0;

                if (X.getQualis() == QUALIS.A1)         // nenhum deles é o melhor
                    return -1;
                if (Y.getQualis() == QUALIS.A1)
                    return 1;

                if (X.getQualis() == QUALIS.A2)
                    return -1;
                if (Y.getQualis() == QUALIS.A2)
                    return 1;

                if (X.getQualis() == QUALIS.B1)
                    return -1;
                if (Y.getQualis() == QUALIS.B1)
                    return 1;

                if (X.getQualis() == QUALIS.B2)
                    return -1;
                if (Y.getQualis() == QUALIS.B2)
                    return 1;

                if (X.getQualis() == QUALIS.B3)
                    return -1;
                if (Y.getQualis() == QUALIS.B3)
                    return 1;

                if (X.getQualis() == QUALIS.B4)
                    return -1;
                if (Y.getQualis() == QUALIS.B4)
                    return 1;

                if (X.getQualis() == QUALIS.B5)
                    return -1;
                if (Y.getQualis() == QUALIS.B5)
                    return 1;

                if (X.getQualis() == QUALIS.C)
                    return -1;

                //if (Y.getQualis() == QUALIS.C)
                return 1;

                if (X.getQualis() == QUALIS.Vazio)     // vazio pra trás
                    return 1;
                if (Y.getQualis() == QUALIS.Vazio)
                    return -1;
                return 0;*/
            }
        }

        public class SortCoautores : IComparer<Artigo>  // com mais coautores vem antes
        {
            public int Compare(Artigo X, Artigo Y)
            {
                if (X.getNCoautores() == Y.getNCoautores())
                    return 0;
                if (X.getNCoautores() > Y.getNCoautores())
                    return -1;
                return 1;
            }
        }
    }
}