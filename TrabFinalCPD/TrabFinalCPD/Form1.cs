﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace TrabFinalCPD
{
    public partial class Form1 : Form
    {
        bool firstTime = true;

        List<Pesquisador> listaPesq = new List<Pesquisador>();
        Pesquisador selecionado;
        Artigo artselecionado;

        //List<Artigo> todosArt = new List<Artigo>();

        string programPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Indexador CPD\";
        
        public Form1()
        {
            if (System.IO.Directory.Exists(programPath))
            {
                firstTime = false;

            }

            InitializeComponent();

            if (!firstTime)
                carregarBaseDados();
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja fechar?", "Sair", MessageBoxButtons.YesNo,
                    MessageBoxIcon.None) == DialogResult.Yes)
                Application.Exit();
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String message = "Trabalho desenvolvido  para a disciplina de Classificação e Pesquisa de Dados, " +
                            "ministrada pelo professor Leandro Krug Wives, pelos alunos Khin Baptista e Leonardo " +
                            "Tagliaro, turma A de 2013/1.\r\n\r\n\tEm construção.";

            MessageBox.Show(message, "Indexador e Analisador de Currículos Lattes", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void dataGridArtigos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridArtigos.Rows.Count == 0)
                return;

            String chave = dataGridArtigos.SelectedCells[0].Value.ToString();

            bool isIndex = false, isDiferente = true;
            int i;
            Artigo dummy;

            /*try
            {
                i = int.Parse(chave);
                isIndex = true;
            }
            catch (Exception b)
            {
                isIndex = false;
            }

            if (isIndex)
            {
                dummy = procurarArtigo(selecionado.offsetArtigos[0][int.Parse(chave) - 1]);
                if (artselecionado != null)
                    if (artselecionado.Equals(dummy))
                        isDiferente = false;
            }
            else
            {*/
                i = procuraArtigoTitulo(dataGridArtigos.SelectedCells[0].Value.ToString());
                dummy = procurarArtigo(i);

                if (artselecionado != null)
                    if (artselecionado.Equals(dummy))
                        isDiferente = false;
            //}

            if (isDiferente)
            {
                dataGridRelacionados.Rows.Clear();
                artselecionado = dummy;

                //Console.WriteLine("Artigo Selecionado: " + dummy.getTitulo());

                foreach (int offset in selecionado.offsetArtigos[0])
                {
                    int nkeywords = 0;
                    Artigo comp = procurarArtigo(offset);
                    //Console.WriteLine("Pesquisador: " + selecionado.nome + "   Artigo: " + comp.getTitulo());
                    foreach(string keyword in comp.palavraChave) 
                    {
                        //Console.WriteLine("keyword:" + keyword);
                        if (keyword != "0" && dummy.palavraChave.Contains(keyword) && dummy.getTitulo() != comp.getTitulo())
                        {
                            nkeywords++;
                        }
                    }
                    if( nkeywords > 0 )
                        dataGridRelacionados.Rows.Add(comp.ToArrayString());
                }
            }
        }

        private void dataGridPesquisadores_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridPesquisadores.Rows.Count == 0)  // bugava quando clicava no header antes de carregar os pesquisadores
                return;

            String chave = dataGridPesquisadores.SelectedCells[0].Value.ToString();
            bool isIndex = false, isDiferente = true;
            int i;
            Pesquisador dummy;

            try
            {
                i = int.Parse(chave);
                isIndex = true;
            }
            catch (Exception b)
            {
                isIndex = false;
            }
            //Console.WriteLine(listaPesq.Count);
            if (isIndex)
            {
                dummy = listaPesq[int.Parse(chave) - 1];
                if (selecionado != null)
                    if (selecionado.Equals(dummy))
                        isDiferente = false;
            }
            else
            {
                // i = valor armazenado na célula 0 da linha selecionada
                chave = dataGridPesquisadores.Rows[e.RowIndex].Cells[e.ColumnIndex - 1].Value.ToString();
                i = int.Parse(chave);
                dummy = listaPesq[i - 1];

                if (selecionado != null)
                    if (selecionado.Equals(dummy))
                        isDiferente = false;
            }

            if (isDiferente)
            {
                dataGridArtigos.Rows.Clear();
                dataGridRelacionados.Rows.Clear();
                selecionado = dummy;

                foreach (int offset in selecionado.offsetArtigos[0])
                {
                    dataGridArtigos.Rows.Add(procurarArtigo(offset).ToArrayString());
                    //dataGridArtigos.Rows.Add(todosArt[offset].ToArrayString());
                }
            }
        }

        private void buttonSortPesqAZ_Click(object sender, EventArgs e)
        {
            int i = 1;

            listaPesq.Sort();

            dataGridPesquisadores.Rows.Clear();

            foreach (Pesquisador p in listaPesq)
            {
                dataGridPesquisadores.Rows.Add(i.ToString(), p.nome);
                i++;
            }
        }

        private void buttonSortPesqZA_Click(object sender, EventArgs e)
        {
            int i = 1;

            listaPesq.Sort(new Pesquisador.SortNameZA());

            dataGridPesquisadores.Rows.Clear();
            foreach (Pesquisador p in listaPesq)
            {
                dataGridPesquisadores.Rows.Add(i.ToString(), p.nome);
                i++;
            }
        }

        private void carregarNovoCurrículoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!firstTime) // avisa o usuário que já existem arquivos carregados
            {
                if (MessageBox.Show("Novos arquivos serão carregados à base já existente. Deseja continuar?", "Novos pesquisadores",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;

                foreach (Pesquisador p in listaPesq)
                {
                    p.artigos = new List<Artigo>();
                    foreach (int j in p.offsetArtigos[0])
                    {
                        p.artigos.Add(procurarArtigo(j));
                        //p.artigos.Add(todosArt[j]);
                    }
                }
            }
            else
            {               // se for a primeira vez, cria os arquivos.
                System.IO.Directory.CreateDirectory(programPath);
                firstTime = false;
            }

            // abre a janela pra escolher os arquivos
            dialogNovoXML.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            dialogNovoXML.Filter = "XML files|*.xml";
            if (dialogNovoXML.ShowDialog() == DialogResult.Cancel)
                return;

            List<string> pathXML = dialogNovoXML.FileNames.ToList<string>();    // passa todos os arquivos eselecionados pra uma lista

            if (pathXML.Count == 0)     // Nunca vai entrar na verdade, mas não custa garantir: se a lista estiver vazia, deu algum erro.
            {
                MessageBox.Show("Arquivo inválido.", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridPesquisadores.Rows.Clear();
           // listaPesq.Clear();  // a toda vez que arquivos novos forem carregados, a lista é zerada (não é pra ser assim)
            
            // Leitura dos arquivos XML:
            XmlReader file;
            string nomePesq = "";
            List<Artigo> listaArt;

            // Variáveis "dummy":
            Pesquisador pesqDummy;
            Artigo artigoDummy;
            string tituloDummy = "";
            string naturezaDummy = "";
            int contaCoautores = 0;
            string[] keywords = new string[6];
            string qualisDummy = "";

            foreach (string newXML in pathXML)  // para cada arquivo selecionado
            {
                file = XmlReader.Create(new StreamReader(newXML, Encoding.GetEncoding("UTF-8")));   // abre o arquivo
                listaArt = new List<Artigo>();  // limpa a lista de artigos do pesquisador anterior

                while (file.Read())     // enquanto a leitura for bem-sucedida
                {
                    if (file.NodeType == XmlNodeType.Element && file.Name == "DADOS-GERAIS")
                        nomePesq = file.GetAttribute("NOME-COMPLETO");

                    while (file.NodeType == XmlNodeType.Element && file.Name == "TRABALHO-EM-EVENTOS")  // Lê todas as conferências
                    {
                        file.Read();
                        if (file.Name == "DADOS-BASICOS-DO-TRABALHO")
                        {
                            tituloDummy = file.GetAttribute("TITULO-DO-TRABALHO");
                            naturezaDummy = file.GetAttribute("NATUREZA");
                        }

                        file.Read();
                        if (file.Name == "DETALHAMENTO-DO-TRABALHO")
                            qualisDummy = file.GetAttribute(1);

                        qualisDummy = fillQualis(0, qualisDummy);
                        

                        file.Read();
                        while (file.Name == "AUTORES")
                        {
                            contaCoautores++;
                            file.Read();
                        }

                        if (file.Name == "PALAVRAS-CHAVE")
                        {
                            for (int i = 0; i < 6; i++)
                            {
                                keywords[i] = file.GetAttribute(i);
                                Console.WriteLine(keywords[i]);
                            }
                        }

                        artigoDummy = new Artigo(tituloDummy, 0, naturezaDummy, qualisDummy, contaCoautores, keywords);
                        listaArt.Add(artigoDummy);

                        //if (!existeArtigo(artigoDummy.getTitulo()))
                        //{
                           //escreveArtigo(artigoDummy);
                           // todosArt.Add(artigoDummy);
                        //}

                        contaCoautores = 0;
                        keywords = new string[6];
                    }

                    while (file.NodeType == XmlNodeType.Element && file.Name == "ARTIGO-PUBLICADO")     // Lê todos os artigos
                    {
                        file.Read();
                        if (file.Name == "DADOS-BASICOS-DO-ARTIGO")
                        {
                            tituloDummy = file.GetAttribute("TITULO-DO-ARTIGO");
                            naturezaDummy = file.GetAttribute("NATUREZA");
                        }

                        file.Read();
                        if (file.Name == "DETALHAMENTO-DO-ARTIGO")
                            qualisDummy = file.GetAttribute(0);

                        qualisDummy = fillQualis(1, qualisDummy);

                        file.Read();

                        while (file.Name == "AUTORES")
                        {
                                contaCoautores++;
                                file.Read();
                        }

                        if (file.Name == "PALAVRAS-CHAVE")
                        {
                            for (int i = 0; i < 6; i++)
                            {
                                keywords[i] = file.GetAttribute(i);
                                Console.WriteLine(keywords[i]);
                            }
                        }

                        artigoDummy = new Artigo(tituloDummy, 1, naturezaDummy, qualisDummy, contaCoautores, keywords);

                        //para verificar se o erro está no construtor
                        //Console.WriteLine("\n\n=========\nverifica erro construtor");
                        //for (int i = 0; i < 6; i++) Console.WriteLine(artigoDummy.palavraChave[i]);
                        //Console.WriteLine("\n==============\n\n");

                        listaArt.Add(artigoDummy);

                        //if (!existeArtigo(artigoDummy.getTitulo()))
                        //{
                            //escreveArtigo(artigoDummy);
                            //todosArt.Add(artigoDummy);
                        //}

                        contaCoautores = 0;
                        keywords = new string[6];

                    }
                }

                pesqDummy = new Pesquisador(nomePesq, listaArt);
                if (!listaPesq.Contains(pesqDummy))
                {
                    listaPesq.Add(pesqDummy);
                }

                file.Close();
            }

            List<Pesquisador> listaPesqRef = new List<Pesquisador>();

            //foreach (Pesquisador pesq in listaPesq)
                //listaPesqRef.Add(pesq);

            listaPesq.Sort();
            //todosArt.Sort();    // Organiza por titulo

            int numartigos = 0;
            BinaryWriter arqArt = new BinaryWriter(File.Open(programPath + @"\artigos.bin", FileMode.Create));
            arqArt.Close();

            foreach (Pesquisador pesq in listaPesq)
            {

                    List<Artigo> listRef = new List<Artigo>();

                    
                    foreach (Artigo art in pesq.artigos)
                    {
                        //Console.WriteLine("\nArtigo:\n==========\nTitulo: " + art.getTitulo() + "\nPalavras-chave:\n");
                        //for(int i=0; i<6; i++) Console.WriteLine(art.palavraChave[i]);

                        // nesse ponto, as palavras-chave já chegam iguais

                        escreveArtigo(art);
                        listRef.Add(art);
                    }

                    pesq.artigos.Sort();    // organiza por nome dos artigos
                    pesq.offsetArtigos[0] = new List<int>();
                    foreach (Artigo art in pesq.artigos)
                    {
                        int index = 0;
                        foreach(Artigo artref in listRef)
                        {
                            if (art == artref)
                                pesq.offsetArtigos[0].Add(numartigos + index);

                            index++;
                        }
                    }

                    pesq.artigos.Sort(new Artigo.SortTipo());
                    pesq.offsetArtigos[1] = new List<int>();
                    foreach (Artigo art in pesq.artigos)
                    {
                        int index = 0;
                        foreach (Artigo artref in listRef)
                        {
                            if (art == artref)
                                pesq.offsetArtigos[1].Add(numartigos + index);

                            index++;
                        }
                    }

                    pesq.artigos.Sort(new Artigo.SortNatureza());
                    pesq.offsetArtigos[2] = new List<int>();
                    foreach (Artigo art in pesq.artigos)
                    {
                        int index = 0;
                        foreach (Artigo artref in listRef)
                        {
                            if (art == artref)
                                pesq.offsetArtigos[2].Add(numartigos + index);

                            index++;
                        }
                    }

                    pesq.artigos.Sort(new Artigo.SortQualis());
                    pesq.offsetArtigos[3] = new List<int>();
                    foreach (Artigo art in pesq.artigos)
                    {
                        int index = 0;
                        foreach (Artigo artref in listRef)
                        {
                            if (art == artref)
                                pesq.offsetArtigos[3].Add(numartigos + index);

                            index++;
                        }
                    }

                    pesq.artigos.Sort(new Artigo.SortCoautores());
                    pesq.offsetArtigos[4] = new List<int>();
                    foreach (Artigo art in pesq.artigos)
                    {
                        int index = 0;
                        foreach (Artigo artref in listRef)
                        {
                            if (art == artref)
                                pesq.offsetArtigos[4].Add(numartigos + index);

                            index++;
                        }
                    }

                    numartigos = numartigos + pesq.artigos.Count();

                    pesq.artigos.Clear();   // apaga a lista de artigos em memória (não precisa mais dela)
            }

            fillGridPesq();

            escreverBaseDados();
        }

        private string fillQualis(int flag, string nome)
        {
            string qualis = "Vazio";
            int index = -1;
            int size;

            if (!System.IO.File.Exists(programPath + "conferencias.csv") || !System.IO.File.Exists(programPath + "periodicos.csv"))
            {       // pedir pra dar o endereço certo
                dialogNovoXML.Filter = "CSV files|*.csv";
                if (dialogNovoXML.ShowDialog() == DialogResult.Cancel) return "Vazio";

                string[] path = dialogNovoXML.FileNames;

                if (path[0].EndsWith("conferencias.csv"))
                {
                    System.IO.File.Copy(path[0], programPath + "conferencias.csv");
                    System.IO.File.Copy(path[1], programPath + "periodicos.csv");
                }
                else
                {
                    System.IO.File.Copy(path[1], programPath + "conferencias.csv");
                    System.IO.File.Copy(path[0], programPath + "periodicos.csv");
                }
            }

            bool found = false;

            if (flag == 0)  // evento
            {
                string[] qualisEvento = System.IO.File.ReadAllLines(programPath + "conferencias.csv");
                size = qualisEvento.Length;

                while (!found && index < size - 1)
                {
                    index++;
                    found = qualisEvento[index].Contains(nome);

                    if (!found)
                        found = nome.Contains(qualisEvento[0]);
                }

                if (found)
                {
                    string[] linhaCerta = qualisEvento[index].Split(';');

                    qualis = linhaCerta[linhaCerta.Length - 1];
                }
            }
            else            // artigo
            {
                string[] qualisArtigo = System.IO.File.ReadAllLines(programPath + "periodicos.csv");
                size = qualisArtigo.Length;

                while (!found && index < size - 1)
                {
                    index++;
                    found = qualisArtigo[index].Contains(nome);

                }

                if (found)
                {
                    string[] linhaCerta = qualisArtigo[index].Split(';');

                    qualis = linhaCerta[linhaCerta.Length - 1];
                }
            }

            return qualis;
        }

        private void fillGridPesq()
        {
            int i = 1;
            foreach (Pesquisador pesq in listaPesq)
            {
                dataGridPesquisadores.Rows.Add(new string[] { i.ToString(), pesq.nome});
                i++;
            }
        }

        private void deletarArquivosCriadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (firstTime)
                return;

            dataGridRelacionados.Rows.Clear();
            dataGridArtigos.Rows.Clear();
            dataGridPesquisadores.Rows.Clear();
            listaPesq.Clear();
            //todosArt.Clear();

            System.IO.File.Delete(programPath + "pesquisadores.bin");
            System.IO.File.Delete(programPath + "artigos.bin");
            System.IO.File.Delete(programPath + "periodicos.csv");
            System.IO.File.Delete(programPath + "conferencias.csv");
            System.IO.Directory.Delete(programPath);
            firstTime = true;

            MessageBox.Show("Arquivos deletados", "", MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        private void escreveArtigo(Artigo a)
        {
            BinaryWriter arqArt = new BinaryWriter(File.Open(programPath + @"\artigos.bin", FileMode.Append), Encoding.UTF8);

            string titulo = a.getTitulo();

            int tipo, natureza;

            string qualis = "";
            string keyDummy;

            if(a.getTipo() == Artigo.tipoArtigo.Periodico) tipo = 0;
            else tipo = 1;

            if(a.getNatureza() == Artigo.naturezaArtigo.Completo) natureza = 0;
            else if(a.getNatureza() == Artigo.naturezaArtigo.Estendido) natureza = 1;
            else natureza = 2;

            if(a.getQualis() == Artigo.QUALIS.A1) qualis = "A1";
            else if(a.getQualis() == Artigo.QUALIS.A2) qualis = "A2";
            else if(a.getQualis() == Artigo.QUALIS.B1) qualis = "B1";
            else if(a.getQualis() == Artigo.QUALIS.B2) qualis = "B2";
            else if(a.getQualis() == Artigo.QUALIS.B3) qualis = "B3";
            else if(a.getQualis() == Artigo.QUALIS.B4) qualis = "B4";
            else if(a.getQualis() == Artigo.QUALIS.B5) qualis = "B5";
            else if(a.getQualis() == Artigo.QUALIS.C) qualis = "C";
            else if(a.getQualis() == Artigo.QUALIS.Vazio) qualis = "Vazio";

            arqArt.Write(a.getTitulo());
            arqArt.Write(tipo);
            arqArt.Write(natureza);
            arqArt.Write(qualis);
            arqArt.Write(a.getNCoautores());

            for (int i = 0; i < 6; i++)
            {
                keyDummy = a.palavraChave[i];
                arqArt.Write(keyDummy);
            }

            arqArt.Close();

            return;
        }

        private int offsetArtigo(string titulo)
        {
            BinaryReader arqArt = new BinaryReader(File.Open(programPath + @"\artigos.bin", FileMode.Open));

            int n = 0;

            string tituloDummy = "", qualisDummy;
            int tipoDummy, naturezaDummy, autoresDummy;
            string[] keywordsDummy = new string[6];

            while (arqArt.PeekChar() != -1)
            {
                tituloDummy = arqArt.ReadString();
                tipoDummy = arqArt.ReadInt32();
                naturezaDummy = arqArt.ReadInt32();
                qualisDummy = arqArt.ReadString();
                autoresDummy = arqArt.ReadInt32();
                for (int i = 0; i < 6; i++)
                {
                    keywordsDummy[i] = arqArt.ReadString();
                }

                if (tituloDummy == titulo)
                {
                    arqArt.Close();
                    return n;
                }

                n++;
            }

            arqArt.Close();
            return -1;
        }

        private int procuraArtigoTitulo(string titulo)
        {
            int num = 0;
            if (!System.IO.File.Exists(programPath + @"\artigos.bin")) return -1;
            BinaryReader arqArt = new BinaryReader(File.Open(programPath + @"\artigos.bin", FileMode.Open));

            string tituloDummy = "", qualisDummy;
            int tipoDummy, naturezaDummy, autoresDummy;
            string[] keywordsDummy = new string[6];

            while (arqArt.PeekChar() != -1)
            {
                tituloDummy = arqArt.ReadString();
                tipoDummy = arqArt.ReadInt32();
                naturezaDummy = arqArt.ReadInt32();
                qualisDummy = arqArt.ReadString();
                autoresDummy = arqArt.ReadInt32();
                for (int i = 0; i < 6; i++)
                {
                    keywordsDummy[i] = arqArt.ReadString();
                }

                if (tituloDummy == titulo)
                {
                    arqArt.Close();
                    return num;
                }
                num++;
            }

            arqArt.Close();
            return -1;
        }

        private Artigo procurarArtigo(int n)
        {
            BinaryReader arqArt = new BinaryReader(File.Open(programPath + @"\artigos.bin", FileMode.Open));

            string tituloDummy = "", qualisDummy = "";
            int tipoDummy = 0, naturezaDummy = 0, autoresDummy = 0;
            string[] keywordsDummy = new string[6];

            while (n >= 0)
            {
                tituloDummy = arqArt.ReadString();
                tipoDummy = arqArt.ReadInt32();
                naturezaDummy = arqArt.ReadInt32();
                qualisDummy = arqArt.ReadString();
                autoresDummy = arqArt.ReadInt32();
                for (int i = 0; i < 6; i++)
                {
                    keywordsDummy[i] = arqArt.ReadString();
                    //Console.WriteLine("Keyword lida do arquivo: " + keywordsDummy[i]);
                }
                n--;
            }

            Artigo artCarregado = new Artigo(tituloDummy, tipoDummy, naturezaDummy, qualisDummy, autoresDummy, keywordsDummy);

            arqArt.Close();
            return artCarregado;
        }

        private void escreverBaseDados()
        {
            /*BinaryWriter arqArt = new BinaryWriter(File.Open(programPath + @"\artigos.bin", FileMode.Create));

            arqArt.Write(todosArt.Count);

            foreach (Artigo a in todosArt)
            {
                string titulo = a.getTitulo();

                int tipo, natureza;

                string qualis = "Vazio";

                if(a.getTipo() == Artigo.tipoArtigo.Periodico) tipo = 0;
                else tipo = 1;

                if(a.getNatureza() == Artigo.naturezaArtigo.Completo) natureza = 0;
                else if(a.getNatureza() == Artigo.naturezaArtigo.Estendido) natureza = 1;
                else natureza = 2;

                if(a.getQualis() == Artigo.QUALIS.A1) qualis = "A1";
                else if(a.getQualis() == Artigo.QUALIS.A2) qualis = "A2";
                else if(a.getQualis() == Artigo.QUALIS.B1) qualis = "B1";
                else if(a.getQualis() == Artigo.QUALIS.B2) qualis = "B2";
                else if(a.getQualis() == Artigo.QUALIS.B3) qualis = "B3";
                else if(a.getQualis() == Artigo.QUALIS.B4) qualis = "B4";
                else if(a.getQualis() == Artigo.QUALIS.B5) qualis = "B5";
                else if(a.getQualis() == Artigo.QUALIS.C) qualis = "C";
                else if(a.getQualis() == Artigo.QUALIS.Vazio) qualis = "Vazio";

                arqArt.Write(a.getTitulo());
                arqArt.Write(tipo);
                arqArt.Write(natureza);
                arqArt.Write(qualis);
                arqArt.Write(a.getNCoautores());

                for (int i = 0; i < 6; i++)
                    arqArt.Write(a.palavraChave[i]);
            }

            arqArt.Close();*/

            BinaryWriter arqPesq = new BinaryWriter(File.Open(programPath + @"\pesquisadores.bin", FileMode.Create));

            arqPesq.Write(listaPesq.Count);

            foreach (Pesquisador p in listaPesq)
            {
                arqPesq.Write(p.nome);
                arqPesq.Write(p.offsetArtigos[0].Count);

                for (int j = 0; j < 5; j++)
                    foreach (int i in p.offsetArtigos[j]) arqPesq.Write(i);
            }

            arqPesq.Close();
        }

        private void escreverPesquisador(Pesquisador p)
        {
            BinaryWriter arqPesq = new BinaryWriter(File.Open(programPath + @"\pesquisadores.bin", FileMode.Create));

            arqPesq.Write(listaPesq.Count);

            arqPesq.Write(p.nome);
            arqPesq.Write(p.artigos.Count);

            for (int j = 0; j < 5; j++)
                foreach (int i in p.offsetArtigos[j]) arqPesq.Write(i);

            arqPesq.Close();
        }

        private void carregarBaseDados()
        {
            // Esboço da leitura de arquivos =========================================================================================== //

            if (firstTime || !System.IO.File.Exists(programPath + @"\pesquisadores.bin"))
            {
                //MessageBox.Show("Arquivo inválido.", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //zerar abaixo a lista de artigos e a de pesquisadores, para carregar dos arquivos
            listaPesq.Clear();
            //todosArt.Clear();

           // long numArtigos = 0;
            // realiza leitura dos artigos
            /*using(BinaryReader arqArt = new BinaryReader(File.Open(programPath + @"\artigos.bin", FileMode.Open)))
            {
                int numart = arqArt.ReadInt32();
                // variáveis dummy:
                string tituloDummy, qualisDummy;
                int tipoDummy, naturezaDummy, autoresDummy;
                string[] keywordsDummy = new string[6];

                while (numart > 0)
                {
                    tituloDummy = arqArt.ReadString();
                    tipoDummy = arqArt.ReadInt32();
                    naturezaDummy = arqArt.ReadInt32();
                    qualisDummy = arqArt.ReadString();
                    autoresDummy = arqArt.ReadInt32();
                    for (int i = 0; i < 6; i++)
                        keywordsDummy[i] = arqArt.ReadString();

                    todosArt.Add(new Artigo(tituloDummy, tipoDummy, naturezaDummy, qualisDummy, autoresDummy, keywordsDummy));
                    numart--;// = numart - 1;
                    //  numArtigos++;
                }

            }*/

            // realiza leitura dos pesquisadores
            using(BinaryReader arqPesq = new BinaryReader(File.Open(programPath + @"\pesquisadores.bin", FileMode.Open)))
            {
                int numpesq = arqPesq.ReadInt32();
                while (numpesq > 0)
                {
                    string nome;
                    int numeroartigos;

                    // inicia lista vazia
                    List<int>[] artlistPesq = new List<int>[5];

                    nome = arqPesq.ReadString();
                    numeroartigos = arqPesq.ReadInt32();

                    for (int j = 0; j < 5; j++)
                    {
                        artlistPesq[j] = new List<int>();

                        for (int i = 0; i < numeroartigos; i++)
                            artlistPesq[j].Add(arqPesq.ReadInt32());
                    }

                    listaPesq.Add(new Pesquisador(nome, artlistPesq));
                    numpesq--;// = numpesq - 1;
                }

            }

            fillGridPesq();

        // carrega arquivo de artigos

        // itera sobre todos os pesquisadores da lista de pesquisadores

            /*foreach (Pesquisador p in listaPesq)
            {
                // a fazer: caso o pesquisador já exista, é necessário apenas modificar o registro
                BinaryWriter arqWPesq = new BinaryWriter(File.Open(programPath + @"\pesquisadores.bin", FileMode.Open));
                BinaryWriter arqWArt = new BinaryWriter(File.Open(programPath + @"\artigos.bin", FileMode.Open));

                // escreve informações do pesquisador menos lista de artigos no arquivo de pesquisadores
                arqWPesq.Write(p.nome);
                //arqPesq.Write(p.);
                arqWPesq.Write(p.artigos.Count());

                foreach (Artigo a in p.artigos)
                {
                    // cada artigo da lista de artigos é inserido no arquivo global de artigos, caso o artigo ainda não exista
                    //     -> se o artigo existir, o id retornado deve ser obtido do arquivo global
                    //     -> se o artigo não existir, o id retornado

                }
            }*/

            // ================================================================================================================= //
        }

        private void buttonSortArtTitulo_Click(object sender, EventArgs e)
        {
            if (selecionado == null)
                return;

            dataGridArtigos.Rows.Clear();

            foreach (int index in selecionado.offsetArtigos[0])
                dataGridArtigos.Rows.Add(procurarArtigo(index).ToArrayString());
                //dataGridArtigos.Rows.Add(todosArt[index].ToArrayString());
        }

        private void buttonSortArtTipo_Click(object sender, EventArgs e)
        {
            if (selecionado == null)
                return;

            dataGridArtigos.Rows.Clear();

            foreach (int index in selecionado.offsetArtigos[1])
                dataGridArtigos.Rows.Add(procurarArtigo(index).ToArrayString());
                //dataGridArtigos.Rows.Add(todosArt[index].ToArrayString());
        }

        private void buttonSortArtNatureza_Click(object sender, EventArgs e)
        {
            if (selecionado == null)
                return;

            dataGridArtigos.Rows.Clear();

            foreach (int index in selecionado.offsetArtigos[2])
                dataGridArtigos.Rows.Add(procurarArtigo(index).ToArrayString());
        }

        private void buttonSortArtQualis_Click(object sender, EventArgs e)
        {
            if (selecionado == null)
                return;

            dataGridArtigos.Rows.Clear();

            foreach (int index in selecionado.offsetArtigos[3])
                dataGridArtigos.Rows.Add(procurarArtigo(index).ToArrayString());
        }

        private void buttonSortArtCoautores_Click(object sender, EventArgs e)
        {
            if (selecionado == null)
                return;

            dataGridArtigos.Rows.Clear();

            foreach (int index in selecionado.offsetArtigos[4])
                dataGridArtigos.Rows.Add(procurarArtigo(index).ToArrayString());
        }
    }
}
