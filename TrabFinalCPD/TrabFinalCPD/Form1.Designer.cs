﻿namespace TrabFinalCPD
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.arquivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carregarNovoCurrículoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deletarArquivosCriadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridPesquisadores = new System.Windows.Forms.DataGridView();
            this.key = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pesquisador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridArtigos = new System.Windows.Forms.DataGridView();
            this.nomeArtigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoArtigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.naturezaArtigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qualisArtigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coautoresArtigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonSortPesqAZ = new System.Windows.Forms.Button();
            this.buttonSortPesqZA = new System.Windows.Forms.Button();
            this.dialogNovoXML = new System.Windows.Forms.OpenFileDialog();
            this.dataGridRelacionados = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonSortArtTitulo = new System.Windows.Forms.Button();
            this.buttonSortArtTipo = new System.Windows.Forms.Button();
            this.buttonSortArtNatureza = new System.Windows.Forms.Button();
            this.buttonSortArtQualis = new System.Windows.Forms.Button();
            this.buttonSortArtCoautores = new System.Windows.Forms.Button();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPesquisadores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArtigos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRelacionados)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arquivoToolStripMenuItem,
            this.sobreToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1094, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // arquivoToolStripMenuItem
            // 
            this.arquivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.carregarNovoCurrículoToolStripMenuItem,
            this.deletarArquivosCriadosToolStripMenuItem,
            this.toolStripMenuItem1,
            this.sairToolStripMenuItem});
            this.arquivoToolStripMenuItem.Name = "arquivoToolStripMenuItem";
            this.arquivoToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.arquivoToolStripMenuItem.Text = "&Arquivo";
            // 
            // carregarNovoCurrículoToolStripMenuItem
            // 
            this.carregarNovoCurrículoToolStripMenuItem.Name = "carregarNovoCurrículoToolStripMenuItem";
            this.carregarNovoCurrículoToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.carregarNovoCurrículoToolStripMenuItem.Text = "In&serir novos pesquisadores";
            this.carregarNovoCurrículoToolStripMenuItem.Click += new System.EventHandler(this.carregarNovoCurrículoToolStripMenuItem_Click);
            // 
            // deletarArquivosCriadosToolStripMenuItem
            // 
            this.deletarArquivosCriadosToolStripMenuItem.Name = "deletarArquivosCriadosToolStripMenuItem";
            this.deletarArquivosCriadosToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.deletarArquivosCriadosToolStripMenuItem.Text = "&Deletar arquivos";
            this.deletarArquivosCriadosToolStripMenuItem.Click += new System.EventHandler(this.deletarArquivosCriadosToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(216, 6);
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.sairToolStripMenuItem.Text = "&Fechar";
            this.sairToolStripMenuItem.Click += new System.EventHandler(this.sairToolStripMenuItem_Click);
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.sobreToolStripMenuItem.Text = "Sobre";
            this.sobreToolStripMenuItem.Click += new System.EventHandler(this.sobreToolStripMenuItem_Click);
            // 
            // dataGridPesquisadores
            // 
            this.dataGridPesquisadores.AllowUserToAddRows = false;
            this.dataGridPesquisadores.AllowUserToDeleteRows = false;
            this.dataGridPesquisadores.AllowUserToResizeColumns = false;
            this.dataGridPesquisadores.AllowUserToResizeRows = false;
            this.dataGridPesquisadores.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridPesquisadores.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridPesquisadores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPesquisadores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.key,
            this.Pesquisador});
            this.dataGridPesquisadores.Location = new System.Drawing.Point(12, 60);
            this.dataGridPesquisadores.MultiSelect = false;
            this.dataGridPesquisadores.Name = "dataGridPesquisadores";
            this.dataGridPesquisadores.ReadOnly = true;
            this.dataGridPesquisadores.RowHeadersVisible = false;
            this.dataGridPesquisadores.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridPesquisadores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridPesquisadores.Size = new System.Drawing.Size(240, 558);
            this.dataGridPesquisadores.TabIndex = 2;
            this.dataGridPesquisadores.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridPesquisadores_CellClick);
            // 
            // key
            // 
            this.key.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.key.HeaderText = "#";
            this.key.Name = "key";
            this.key.ReadOnly = true;
            this.key.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.key.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.key.Width = 20;
            // 
            // Pesquisador
            // 
            this.Pesquisador.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Pesquisador.HeaderText = "Pesquisadores";
            this.Pesquisador.Name = "Pesquisador";
            this.Pesquisador.ReadOnly = true;
            this.Pesquisador.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridArtigos
            // 
            this.dataGridArtigos.AllowUserToAddRows = false;
            this.dataGridArtigos.AllowUserToDeleteRows = false;
            this.dataGridArtigos.AllowUserToResizeRows = false;
            this.dataGridArtigos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridArtigos.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridArtigos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridArtigos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomeArtigo,
            this.tipoArtigo,
            this.naturezaArtigo,
            this.qualisArtigo,
            this.coautoresArtigo});
            this.dataGridArtigos.Location = new System.Drawing.Point(258, 60);
            this.dataGridArtigos.MultiSelect = false;
            this.dataGridArtigos.Name = "dataGridArtigos";
            this.dataGridArtigos.ReadOnly = true;
            this.dataGridArtigos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridArtigos.RowHeadersVisible = false;
            this.dataGridArtigos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridArtigos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridArtigos.Size = new System.Drawing.Size(824, 363);
            this.dataGridArtigos.TabIndex = 3;
            this.dataGridArtigos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridArtigos_CellClick);
            // 
            // nomeArtigo
            // 
            this.nomeArtigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nomeArtigo.HeaderText = "Artigos do autor";
            this.nomeArtigo.Name = "nomeArtigo";
            this.nomeArtigo.ReadOnly = true;
            this.nomeArtigo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tipoArtigo
            // 
            this.tipoArtigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.tipoArtigo.HeaderText = "Tipo";
            this.tipoArtigo.Name = "tipoArtigo";
            this.tipoArtigo.ReadOnly = true;
            this.tipoArtigo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tipoArtigo.Width = 34;
            // 
            // naturezaArtigo
            // 
            this.naturezaArtigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.naturezaArtigo.HeaderText = "Natureza";
            this.naturezaArtigo.Name = "naturezaArtigo";
            this.naturezaArtigo.ReadOnly = true;
            this.naturezaArtigo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.naturezaArtigo.Width = 56;
            // 
            // qualisArtigo
            // 
            this.qualisArtigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.qualisArtigo.HeaderText = "QUALIS";
            this.qualisArtigo.Name = "qualisArtigo";
            this.qualisArtigo.ReadOnly = true;
            this.qualisArtigo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.qualisArtigo.Width = 52;
            // 
            // coautoresArtigo
            // 
            this.coautoresArtigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.coautoresArtigo.HeaderText = "# Coautores";
            this.coautoresArtigo.Name = "coautoresArtigo";
            this.coautoresArtigo.ReadOnly = true;
            this.coautoresArtigo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.coautoresArtigo.Width = 71;
            // 
            // buttonSortPesqAZ
            // 
            this.buttonSortPesqAZ.Location = new System.Drawing.Point(12, 27);
            this.buttonSortPesqAZ.Name = "buttonSortPesqAZ";
            this.buttonSortPesqAZ.Size = new System.Drawing.Size(117, 23);
            this.buttonSortPesqAZ.TabIndex = 5;
            this.buttonSortPesqAZ.Text = "A - Z";
            this.buttonSortPesqAZ.UseVisualStyleBackColor = true;
            this.buttonSortPesqAZ.Click += new System.EventHandler(this.buttonSortPesqAZ_Click);
            // 
            // buttonSortPesqZA
            // 
            this.buttonSortPesqZA.Location = new System.Drawing.Point(135, 27);
            this.buttonSortPesqZA.Name = "buttonSortPesqZA";
            this.buttonSortPesqZA.Size = new System.Drawing.Size(117, 23);
            this.buttonSortPesqZA.TabIndex = 6;
            this.buttonSortPesqZA.Text = "Z - A";
            this.buttonSortPesqZA.UseVisualStyleBackColor = true;
            this.buttonSortPesqZA.Click += new System.EventHandler(this.buttonSortPesqZA_Click);
            // 
            // dialogNovoXML
            // 
            this.dialogNovoXML.Multiselect = true;
            this.dialogNovoXML.Title = "Selecione os arquivos";
            // 
            // dataGridRelacionados
            // 
            this.dataGridRelacionados.AllowUserToAddRows = false;
            this.dataGridRelacionados.AllowUserToDeleteRows = false;
            this.dataGridRelacionados.AllowUserToResizeRows = false;
            this.dataGridRelacionados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridRelacionados.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridRelacionados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridRelacionados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dataGridRelacionados.Location = new System.Drawing.Point(258, 429);
            this.dataGridRelacionados.MultiSelect = false;
            this.dataGridRelacionados.Name = "dataGridRelacionados";
            this.dataGridRelacionados.ReadOnly = true;
            this.dataGridRelacionados.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridRelacionados.RowHeadersVisible = false;
            this.dataGridRelacionados.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridRelacionados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridRelacionados.Size = new System.Drawing.Size(824, 189);
            this.dataGridRelacionados.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Artigos relacionados";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn2.HeaderText = "Tipo";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 53;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.HeaderText = "Natureza";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 75;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn4.HeaderText = "QUALIS";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 71;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn5.HeaderText = "# Coautores";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 90;
            // 
            // buttonSortArtTitulo
            // 
            this.buttonSortArtTitulo.Location = new System.Drawing.Point(473, 27);
            this.buttonSortArtTitulo.Name = "buttonSortArtTitulo";
            this.buttonSortArtTitulo.Size = new System.Drawing.Size(117, 23);
            this.buttonSortArtTitulo.TabIndex = 8;
            this.buttonSortArtTitulo.Text = "Título A - Z";
            this.buttonSortArtTitulo.UseVisualStyleBackColor = true;
            this.buttonSortArtTitulo.Click += new System.EventHandler(this.buttonSortArtTitulo_Click);
            // 
            // buttonSortArtTipo
            // 
            this.buttonSortArtTipo.Location = new System.Drawing.Point(596, 27);
            this.buttonSortArtTipo.Name = "buttonSortArtTipo";
            this.buttonSortArtTipo.Size = new System.Drawing.Size(117, 23);
            this.buttonSortArtTipo.TabIndex = 9;
            this.buttonSortArtTipo.Text = "Tipo";
            this.buttonSortArtTipo.UseVisualStyleBackColor = true;
            this.buttonSortArtTipo.Click += new System.EventHandler(this.buttonSortArtTipo_Click);
            // 
            // buttonSortArtNatureza
            // 
            this.buttonSortArtNatureza.Location = new System.Drawing.Point(719, 27);
            this.buttonSortArtNatureza.Name = "buttonSortArtNatureza";
            this.buttonSortArtNatureza.Size = new System.Drawing.Size(117, 23);
            this.buttonSortArtNatureza.TabIndex = 10;
            this.buttonSortArtNatureza.Text = "Natureza";
            this.buttonSortArtNatureza.UseVisualStyleBackColor = true;
            this.buttonSortArtNatureza.Click += new System.EventHandler(this.buttonSortArtNatureza_Click);
            // 
            // buttonSortArtQualis
            // 
            this.buttonSortArtQualis.Location = new System.Drawing.Point(842, 27);
            this.buttonSortArtQualis.Name = "buttonSortArtQualis";
            this.buttonSortArtQualis.Size = new System.Drawing.Size(117, 23);
            this.buttonSortArtQualis.TabIndex = 11;
            this.buttonSortArtQualis.Text = "QUALIS";
            this.buttonSortArtQualis.UseVisualStyleBackColor = true;
            this.buttonSortArtQualis.Click += new System.EventHandler(this.buttonSortArtQualis_Click);
            // 
            // buttonSortArtCoautores
            // 
            this.buttonSortArtCoautores.Location = new System.Drawing.Point(965, 27);
            this.buttonSortArtCoautores.Name = "buttonSortArtCoautores";
            this.buttonSortArtCoautores.Size = new System.Drawing.Size(117, 23);
            this.buttonSortArtCoautores.TabIndex = 12;
            this.buttonSortArtCoautores.Text = "# Coautores";
            this.buttonSortArtCoautores.UseVisualStyleBackColor = true;
            this.buttonSortArtCoautores.Click += new System.EventHandler(this.buttonSortArtCoautores_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 630);
            this.Controls.Add(this.buttonSortArtCoautores);
            this.Controls.Add(this.buttonSortArtQualis);
            this.Controls.Add(this.buttonSortArtNatureza);
            this.Controls.Add(this.buttonSortArtTipo);
            this.Controls.Add(this.buttonSortArtTitulo);
            this.Controls.Add(this.dataGridRelacionados);
            this.Controls.Add(this.buttonSortPesqZA);
            this.Controls.Add(this.buttonSortPesqAZ);
            this.Controls.Add(this.dataGridArtigos);
            this.Controls.Add(this.dataGridPesquisadores);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(700, 500);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trabalho Final CPD";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPesquisadores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArtigos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRelacionados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem arquivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridPesquisadores;
        private System.Windows.Forms.DataGridView dataGridArtigos;
        private System.Windows.Forms.DataGridViewTextBoxColumn key;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pesquisador;
        private System.Windows.Forms.Button buttonSortPesqAZ;
        private System.Windows.Forms.Button buttonSortPesqZA;
        private System.Windows.Forms.ToolStripMenuItem carregarNovoCurrículoToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog dialogNovoXML;
        private System.Windows.Forms.DataGridView dataGridRelacionados;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.ToolStripMenuItem deletarArquivosCriadosToolStripMenuItem;
        private System.Windows.Forms.Button buttonSortArtTitulo;
        private System.Windows.Forms.Button buttonSortArtTipo;
        private System.Windows.Forms.Button buttonSortArtNatureza;
        private System.Windows.Forms.Button buttonSortArtQualis;
        private System.Windows.Forms.Button buttonSortArtCoautores;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeArtigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoArtigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn naturezaArtigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn qualisArtigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn coautoresArtigo;
    }
}

